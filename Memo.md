
# Memo

+ CRF の概要：
    + [Wikipedia: 条件付き確率場](https://ja.wikipedia.org/wiki/条件付き確率場)（[en](https://en.wikipedia.org/wiki/Conditional_random_field)）
+ CRFasRNN
    + http://www.robots.ox.ac.uk/~szheng/crfasrnndemo
        + semantic segmentation デモサイト
    + https://github.com/torrvision/crfasrnn/
        + デモのコード（ただし実装詳細は隠してあり、Caffeの訓練済モデルをダウンロードするようになっている）
    + http://www.robots.ox.ac.uk/~szheng/papers/CRFasRNN.pdf
        + 論文 "Conditional Random Fields as Recurrent Neural Networks"（以下、\[CRFasRNN\]）
        + 概要：
            + semantic segmentation のソリューションの提案
            + CNN と CRF を結合して、end to end で効率よく学習（前段後段ではなく1つのネットワーク）
            + CNN と CRF の両方の強みが活かせる
+ CRF の実装（例）：
    + [tensorflow.contrib.crf](https://github.com/tensorflow/tensorflow/blob/r1.1/tensorflow/contrib/crf/python/ops/crf.py)
        + Tensorflow に含まれる CRF 関係のコード。linear-chain CRF を ネットワークに組み込めるようにしたもの。
        + linear-chain CRF なので、系列データの解析には使用できるが segmentation は×。
    + [CRF.jl](https://github.com/slyrz/CRF.jl)
        + [Julia](https://julialang.org) の linear-chain CRF パッケージ（メンテ止まってる）
        + linear-chain CRF なので以下略
    + [PyStruct](https://pystruct.github.io/)
        + scikit-learn 類似の IF で使える Python パッケージ
        + Multi-class SVM / Multi-label SVM / chainCRF / GraphCRF などなど
        + [Semantic Image Segmentation の例](https://pystruct.github.io/auto_examples/image_segmentation.html)（EdgeFeatureGraphCRF を利用）
+ その他関連事項
    + https://github.com/idofr/pymutohedral_lattice
        + Permutohedral Lattice の 実装
        + Permutohedral Lattice: \[CRFasRNN\] で利用しているフィルタ（？）
    + https://papers.nips.cc/paper/4296-efficient-inference-in-fully-connected-crfs-with-gaussian-edge-potentials.pdf
        + \[CRFasRNN\] で一番多く言及されている論文
        + \[CRFasRNN\] で触れられている「2つのガウスカーネル（＝spatialとbilateral）」の定義が載っている
